$(function(){
    if($('ul.setup-panel').length > 0){
        var navListItems = $('ul.setup-panel li a'),
            allWells = $('.setup-content');

        allWells.hide();

        navListItems.click(function(e)
        {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this).closest('li');

            if (!$item.hasClass('disabled')) {
                navListItems.closest('li').removeClass('active');
                $item.addClass('active');
                allWells.hide();
                $target.show();
            }
        });

        $('ul.setup-panel li.active a').trigger('click');

        $('.requestsec button[id*="activate-step-"]').on('click', function(e) {
            var id = $(this).attr('data-id');
            var nextstepid = $(this).attr('data-next-step');
            $('ul.setup-panel li:eq('+id+')').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-'+nextstepid+'"]').trigger('click');
            $('.requestsec .nav-pills>li>a[href="#step-'+id+'"]').addClass('stepdone');
            $(this).remove();
            var scroll_pos=(0);
            $('html, body').animate({scrollTop:(scroll_pos)}, '2000');
        });
    }


});