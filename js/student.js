$(function(){
    if($('.fb').length>0){
        $('.fb').fancybox()
    }
    $('title').html('LMS');
    $('[data-toggle="tooltip"]').tooltip();
    $('.navicon a').click(function () {
        if($('#togglenaver').is(":visible") ){
            $('#togglenaver').hide();
        }else{
            $('#togglenaver').show();
        }
        $('div#wrappsizer').toggleClass('fullsizer');
    });
    // Homepage Chart
    if($('.chartwrapper').length > 0){
        var presets = window.chartColors;
        var utils = Samples.utils;
        var inputs = {
            min: 0,
            max: 150,
            count: 8,
            decimals: 2,
            continuity: 1
        };

        var options = {
            maintainAspectRatio: false,
            spanGaps: false,
            elements: {
                line: {
                    tension: 0.000001
                }
            },
            plugins: {
                filler: {
                    propagate: false
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 0
                    }
                }]
            }
        };

        var chart_obj = "";
        // reset the random seed to generate the same data for all charts
        utils.srand(8);
        let draw = Chart.controllers.line.prototype.draw;
        Chart.controllers.line = Chart.controllers.line.extend({
            draw: function() {
                draw.apply(this, arguments);
                let ctx = this.chart.chart.ctx;
                let _stroke = ctx.stroke;
                ctx.stroke = function() {
                    ctx.save();
                    ctx.shadowColor = '#4ae7ff';
                    ctx.shadowBlur = 50;
                    ctx.shadowOffsetX = 4;
                    ctx.shadowOffsetY = 4;
                    _stroke.apply(this, arguments)
                    ctx.restore();
                }
            }
        });
        var ctx1 = document.getElementById("chart-2");
        chart_obj = new Chart(ctx1, {
            type: 'line',
            data: {
                labels: ['Jan 2018', 'Apr 2018', 'Mar 2018', 'Nov 2018', 'Feb 2018', 'Oct 2018', 'Dec 2018', 'Sept 2018'],
                datasets: [{
                    backgroundColor: [
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)',
                        'rgba(54,226,255,0.1)'
                    ],
                    borderColor: '#5f97ff',
                    data: [40, 55, 60, 70, 85, 80, 90, 100],
                    label: 'Activities',
                    pointBackgroundColor: '#fff',
                    pointBorderColor: '#78d7ff',
                    pointRadius: 7,
                    pointHoverRadius: 7,
                    pointHoverBackgroundColor: "#5fd0ff",
                    pointHoverBorderColor: "#5fd0ff"
                }]
            },
            options: Chart.helpers.merge(options, {
                responsive: true,
                scales: {
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display:true
                        }
                    }]
                },
                title: {

                    display: false
                },
                tooltips: {
                    backgroundColor: '#5f97ff',
                    titleFontSize: 16,
                    titleFontColor: '#fff',
                    bodyFontColor: '#fff',
                    bodyFontSize: 16,
                    displayColors: false,
                    yAlign: 'bottom',
                    cornerRadius: 20
                }
            })
        });
        var dataToTable = function (dataset) {
            var html = '<table class="table">';
            html += '<thead><tr><th>Months</th>';

            var columnCount = 0;
            jQuery.each(dataset.datasets, function (idx, item) {
                html += '<th>' + item.label + '</th>';
                columnCount += 1;
            });

            html += '</tr></thead>';

            jQuery.each(dataset.labels, function (idx, item) {
                html += '<tr><td>' + item + '</td>';
                for (i = 0; i < columnCount; i++) {
                    html += '<td>' + (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + 'k</td>';
                }
                html += '</tr>';
            });

            html += '</tr><tbody></table>';

            return html;
        };
        $('#wrapper').html(dataToTable(chart_obj.data));
    }
});